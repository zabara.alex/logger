<?php

namespace Leomax\Logger;

interface LoggerInterface
{
    public static function emergency($context);
    public static function alert($context);
    public static function critical($context);
    public static function error($context);
    public static function warning($context);
    public static function notice($context);
    public static function info($context);
    public static function debug($context);
}