<?php

namespace Leomax\Logger\Methods;

/**
 * Interface MethodInterface
 */
interface MethodInterface
{
    public function writeLog($level, $message);
}