<?php
namespace Leomax\Logger\Methods;

use Sentry\EventHint;
use Sentry\ExceptionMechanism;
use Leomax\Logger\Singleton;

class SentryClass extends Singleton implements MethodInterface
{
    protected function __construct()
    {
        \Sentry\init([
            'dsn' => 'https://536860148cca25fbdffe27e2c8e3fb50@o4506110425104384.ingest.sentry.io/4506110427070464',
            // Specify a fixed sample rate
            'traces_sample_rate' => 1.0,
            // Set a sampling rate for profiling - this is relative to traces_sample_rate
            'profiles_sample_rate' => 1.0,
        ]);
    }

    public function writeLog($level, $message)
    {
//        $levelMap = [
//            'emergency' => \Sentry\Severity::fatal(),
//            'critical'  => \Sentry\Severity::error(),
//            'error'     => \Sentry\Severity::error(),
//            'warning'   => \Sentry\Severity::warning(),
//            'info'      => \Sentry\Severity::info(),
//            'notice'    => \Sentry\Severity::info(),
//            'alert'     => \Sentry\Severity::info(),
//            'debug'     => \Sentry\Severity::debug(),
//        ];
//
//        $levelObject = isset($levelMap[$level]) ? $levelMap[$level] : \Sentry\Severity::info();
//
//        \Sentry\captureMessage($message, $levelObject);

        $handled = self::makeAnEducatedGuessIfTheExceptionMaybeWasHandled();

        $hint = EventHint::fromArray([
            'mechanism' => new ExceptionMechanism(ExceptionMechanism::TYPE_GENERIC, $handled),
        ]);

        \Sentry\captureException($message, $hint);
    }

    private static function makeAnEducatedGuessIfTheExceptionMaybeWasHandled()
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 20);

        foreach ($trace as $frameIndex => $frame) {

            if (!isset($frame['class'], $frame['function'])) {
                continue;
            }

            if ($frame['type'] !== '->' || $frame['function'] !== 'report') {
                continue;
            }

            if (!isset($trace[$frameIndex + 1])) {
                continue;
            }

            $nextFrame = $trace[$frameIndex + 1];

            if (isset($nextFrame['class']) || !isset($nextFrame['function']) || $nextFrame['function'] !== 'report') {
                continue;
            }

            return true;
        }
        return false;
    }
}